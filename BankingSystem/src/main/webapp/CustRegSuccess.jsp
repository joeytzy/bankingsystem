<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register Success</title>
<style>
	body{
		background-color: rgb(229, 239, 243);
	}
	a:hover{
		color: red;	
	}
</style>
</head>
<body>
<center>
	<h2>
	<%
	out.println("Congratulations, your account has been successfully created.");
	%>
	</h2>
	<p>Please click <a href="/BankingSystem/custLogin.html">here</a> to login</p>
</center>
</body>
</html>
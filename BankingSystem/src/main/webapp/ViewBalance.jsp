<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page language="java" import="java.util.*" %>
<%@page import="com.dxc.model.CustomerModel"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>View Balance</title>
<style>
	body{
		background-color:rgb(243, 231, 229);
	}
	td{
		font-size: 20px;
	}
	table{
		margin-top: 50px
	}
	.back-btn{
		background-color: rgb(198, 206, 206);
		border-radius: 30px;
		margin-top:20px;
		maring-left:100px;
		border: none;
		height: 25px;
		width: 80px;
		box-shadow: 3px 3px 10px rgb(198, 206, 206),
            -3px -3px 10px rgb(198, 206, 206);
	}
	.back-btn:hover{
		background-color: grey;
		color: white;
	}
</style>
</head>
<body>
<center>
	<h2>Balance</h2>
	<table>
		<tr>
			<td><b>Username: </b><%out.println(session.getAttribute("cusername")); %></td>
		</tr>
		<tr>
			<td><b>Current Balance:</b></td>
			<%Iterator itr;%>
			<% List data= (List)session.getAttribute("custList");
			for (itr=data.iterator(); itr.hasNext(); )
			{
			%>
			<td>$<%=itr.next()%></td>
			<%}%>
		</tr>
	</table>
	<form action="/BankingSystem/custLoginSuccess.jsp">
		<input class="back-btn" type="submit" value="Back">
	</form>
</center>
</body>
</html>
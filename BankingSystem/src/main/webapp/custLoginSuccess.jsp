<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Customer</title>
<style>
	body{
		background-color: rgb(243, 231, 229);
	}
	table{
		padding: 30px
	}
	td{
		padding: 10px;
	}
	input{
		background-color: rgb(231, 152, 136 );
		border-radius: 30px;
		border: none;
		height: 30px;
		width: 200px;
		box-shadow: 2px 3px 5px rgb(231, 152, 136 ),
            -2px -3px 5px rgb(231, 152, 136 );
	}
	
	input:hover{
		background-color: rgb(214, 181, 175);
		color: white;
	}
</style>
</head>
<body>
<center>
	<h2>
	<%
	out.println("Dear "+session.getAttribute("cusername")+", welcome back!");
	%>
	</h2>
	<table>
		<tr>
		<td><input type="button" value="Reset Password" onclick="location.href='/BankingSystem/resetPwd.html'"></td>
		</tr>
		<form action="/BankingSystem/ViewBalance">
			<tr><td><input type="submit" value="Check Balance"></td></tr>
		</form>
		<tr>
		<td><input type="submit" value="Send Money" onclick="location.href='/BankingSystem/sendMoney.html'"></td>
		</tr>
		<tr>
		<td><input type="submit" value="Apply Loan" onclick="location.href='/BankingSystem/applyLoan.html'"></td>
		</tr>
		<tr>
		<td><input type="submit" value="View Transaction" onclick="location.href='/BankingSystem/viewTransaction.html'"></td>
		</tr>
	</table>
</center>

</body>
</html>
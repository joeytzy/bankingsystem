<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page language="java" import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Customer List</title>
<style>
	body{
		background-color: rgb(243, 231, 229);
	}
	.back-btn{
		background-color: rgb(198, 206, 206);
		border-radius: 30px;
		margin-top:20px;
		maring-left:100px;
		border: none;
		height: 25px;
		width: 80px;
		box-shadow: 3px 3px 10px rgb(198, 206, 206),
            -3px -3px 10px rgb(198, 206, 206);
	}
	.back-btn:hover{
		background-color: grey;
		color: white;
	}
</style>
</head>
<body>
<center>
	<h2>Customer List</h2>
	<table border="1" width="600">
		<tr>
		<td><b>Name</b></td>
		<td><b>Username</b></td>
		<td><b>Balance</b></td>
		<td><b>Loan Type</b></td>
		<td><b>Loan Status</b></td>
		<td><b>Security Qn</b></td>
		<td><b>Security Ans</b></td>
		</tr>
		<%Iterator itr;%>
		<% List data= (List)session.getAttribute("data");
		for (itr=data.iterator(); itr.hasNext(); )
		{
		%>
		<tr>
		<td><%=itr.next()%></td>
		<td><%=itr.next()%></td>
		<td><%=itr.next()%></td>
		<td><%=itr.next()%></td>
		<td><%=itr.next()%></td>
		<td><%=itr.next()%></td>
		<td><%=itr.next()%></td>
		</tr>
		<%}%>
	</table>
	<input class="back-btn" type="submit" value="Back" onclick="location.href='/BankingSystem/adminLoginSuccess.jsp'">
</center>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Status Update Success</title>
<style>
	body{
		background-color: rgb(229, 239, 243);
	}
	.done-btn{
		background-color: rgb(198, 206, 206);
		border-radius: 30px;
		margin-top:20px;
		maring-left:100px;
		border: none;
		height: 25px;
		width: 80px;
		box-shadow: 3px 3px 10px rgb(198, 206, 206),
            -3px -3px 10px rgb(198, 206, 206);
	}
	.done-btn:hover{
		background-color: grey;
		color: white;
	}
</style>
</head>
<body>
<center>
	<h3><% out.println("Loan status for " +session.getAttribute("username")+ " is updated"); %></h3>
	<input class="done-btn" type="button" value="Done" onclick="location.href='/BankingSystem/adminLoginSuccess.jsp'">
</center>
</body>
</html>
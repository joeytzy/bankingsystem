<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Admin</title>
<style>
	body{
		background-color: rgb(243, 231, 229);
	}
	table{
		padding: 30px
	}
	td{
		padding: 10px;
	}
	input{
		background-color: rgb(231, 152, 136 );
		border-radius: 30px;
		border: none;
		height: 30px;
		width: 200px;
		box-shadow: 2px 3px 5px rgb(231, 152, 136 ),
            -2px -3px 5px rgb(231, 152, 136 );
	}
	
	input:hover{
		background-color: rgb(214, 181, 175);
		color: white;
	}
</style>
</head>
<body>
<center>
	<h2>
	<%
	out.println("Dear "+session.getAttribute("username")+", welcome back!");
	%>
	</h2>
	<table>
		<form action="/BankingSystem/ViewCustomer">
			<tr><td><input type="submit" value="View Customer"></td></tr>
		</form>
		<form action="/BankingSystem/ViewPending">
			<tr><td><input type="submit" value="View Pending List"></td></tr>
		</form>
		<tr>
		<td><input type="submit" value="Update Status" onclick="location.href='/BankingSystem/updateStatus.html'"></td>
		</tr>
	</table>
</center>
</body>
</html>
package com.dxc.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

public class CustomerModel {
	private String custName;
	private String cusername;
	private int password;
	private int balance;
	private int amount; //transfer amount
	private String loanType;
	private String loanStatus;
	private String securityQn;
	private String securityAns;
	private String receiver; //receiver receiving the money

	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet res = null;
	
	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCusername() {
		return cusername;
	}

	public void setCusername(String cusername) {
		this.cusername = cusername;
	}

	public int getPassword() {
		return password;
	}

	public void setPassword(int password) {
		this.password = password;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public String getLoanStatus() {
		return loanStatus;
	}

	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}

	public String getSecurityQn() {
		return securityQn;
	}

	public void setSecurityQn(String securityQn) {
		this.securityQn = securityQn;
	}

	public String getSecurityAns() {
		return securityAns;
	}

	public void setSecurityAns(String securityAns) {
		this.securityAns = securityAns;
	}
	
	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	
	public CustomerModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded successfully");
			
			con = DriverManager.getConnection("jdbc:mysql://localhost:3307/sample_dxc", "root", "root");
			System.out.println("Connection establish successfully");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public int customerRegister(){
		try {
			String checkDuplicateUsername = "select * from bank_customer where username=?"; 
			pstmt = con.prepareStatement(checkDuplicateUsername);
			pstmt.setString(1, cusername);
			res= pstmt.executeQuery();
			
			while(res.next()) {
				return -1; //username exist
			}
			String s = "insert into bank_customer values(?, ?, ?, ?, ?, ?, ?, ?)";
			pstmt = con.prepareStatement(s);
			
			pstmt.setString(1, custName); 
		    pstmt.setString(2, cusername);
		    pstmt.setInt(3, password);
		    pstmt.setInt(4, balance);
		    pstmt.setString(5, loanType);
		    pstmt.setString(6, loanStatus);
		    pstmt.setString(7, securityQn);
		    pstmt.setString(8, securityAns);
		    
		    pstmt.executeUpdate();
		    return 1;
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	public int customerLogin() {
		try {
			String s = "select * from bank_customer where username=?";
			PreparedStatement pstmt = con.prepareStatement(s);
			pstmt.setString(1, cusername);
	        res= pstmt.executeQuery();
	        
	        while(res.next()) {
	        	if(res.getInt(3) == (password)) {
	        		return 1; //valid credential, login success
		        }
		        else{
		        	return -1; //invalid pwd, login fail
		        }
		    } 
		}catch(Exception e) {
			e.printStackTrace();
		}
	return 0;
	}
	
	public int resetPassword() {
		try {
			String s = "update bank_customer set password=? where username=?";
			pstmt = con.prepareStatement(s);
			pstmt.setInt(1, password);
			pstmt.setString(2, cusername);
			
			int row = pstmt.executeUpdate();
		    return row;
		}catch(Exception e) {
			e.printStackTrace();
		}
	return 0;
	}
	
	public ArrayList<Integer> viewBalance() {
		ArrayList<Integer> custList = new ArrayList<Integer>();
		try {
			String s = "select * from bank_customer where username=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cusername);
			res= pstmt.executeQuery();
		    while(res.next()) {
		    	custList.add(res.getInt(4)); //balance
		    }

		    return custList;
		}catch(Exception e) {
			e.printStackTrace();
		}
	return null;
	}
	  
	public int sendMoney() {
		try {;
			int newBal = 0;
			
			String description = "$"+ amount + " transferred";
			
			String s1 = "select * from bank_customer where username=?";
			pstmt = con.prepareStatement(s1);
			res= pstmt.executeQuery();
			
			while(res.next()) {
				//if current balance > transfer money
				//then deduct the current balance in the bank acc, update new balance and insert transaction record to transaction table
				//else, return error 
				if(res.getInt(4) > amount) {
					newBal = res.getInt(4) - amount; 
					String s2 = "update bank_customer set balance=?  where username=?";
					pstmt = con.prepareStatement(s2);
					pstmt.setInt(1, newBal);
					pstmt.setString(2, cusername);
					pstmt.executeUpdate();
					
					String s3 = "insert into transaction values (?, ?, ?, ?)";
					pstmt = con.prepareStatement(s3);
					pstmt.setString(1, cusername);
					pstmt.setDate(2, java.sql.Date.valueOf(java.time.LocalDate.now()));
					pstmt.setString(3, description);
					pstmt.setString(4, receiver);
					pstmt.executeUpdate();
				}
				else {
					return -1; //current balance is less than intended amount to transfer
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int applyLoan() {
		try {
			String s = "update bank_customer set loan_type=?, loan_status=? where username=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, loanType);
			pstmt.setString(2, "false"); //default set as fault
			pstmt.setString(3, cusername);

			int row = pstmt.executeUpdate();
		    return row;
		    
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}

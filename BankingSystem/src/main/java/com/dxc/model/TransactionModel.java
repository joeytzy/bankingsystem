package com.dxc.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

public class TransactionModel {
	private String cusername;
	private Date transactionDate;
	private Date fromDate;
	private Date toDate;
	private String transactionDetail;
	private String receiver;
	
	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet res = null;
	
	public String getCusername() {
		return cusername;
	}
	public void setCusername(String cusername) {
		this.cusername = cusername;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getTransactionDetail() {
		return transactionDetail;
	}
	public void setTransactionDetail(String transactionDetail) {
		this.transactionDetail = transactionDetail;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	
	public TransactionModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded successfully");
			
			con = DriverManager.getConnection("jdbc:mysql://localhost:3307/sample_dxc", "root", "root");
			System.out.println("Connection establish successfully");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	
	public ArrayList<Object> viewTransaction() {
		ArrayList<Object> transaction = new ArrayList<Object>();
		try {
			String s = "select * from transaction where username=? and transaction_date between ? and ?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cusername);
			pstmt.setDate(2,  new java.sql.Date(fromDate.getTime()));
			pstmt.setDate(3, new java.sql.Date(toDate.getTime()));
			res= pstmt.executeQuery();
			
		    while(res.next()) {
		    	transaction.add(res.getDate(2));
		    	transaction.add(res.getString(3));
		    	transaction.add(res.getString(4));
		    }
		    return transaction;
		}catch(Exception e) {
			e.printStackTrace();
		}
	return null;
	}
	
}

package com.dxc.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AdminModel {
	private String username;
	private String password;
	private String status; //loan status
	private String cusername; //customer username for update status

	Connection con = null;
	PreparedStatement pstmt = null;
	Statement stmt = null;
	ResultSet res = null;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getCusername() {
		return cusername;
	}
	public void setCusername(String cusername) {
		this.cusername = cusername;
	}
	
	public AdminModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded successfully");
			
			con = DriverManager.getConnection("jdbc:mysql://localhost:3307/sample_dxc", "root", "root");
			System.out.println("Connection establish successfully");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
		
	public int adminLogin() {
		try {
			String s = "select * from bank_admin where username=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, username);
	        ResultSet res= pstmt.executeQuery();
	        
	        while(res.next()) {
	        	if(res.getString(2).equals(password)) {
	        		return 1; //login success
		        }
		        else{
		        	return -1; //incorrect pwd, login fail
		        }
		    } 
		}catch(Exception e) {
			e.printStackTrace();
		}
	return 0;
	}
	
	public List<Object> getCustList() {
		List<Object> custList = new ArrayList<Object>();
		try {
			String s = "select * from bank_customer";
			stmt = con.createStatement();
			res = stmt.executeQuery(s);
			
			while(res.next()) {
				custList.add(res.getString(1)); //cust name
				custList.add(res.getString(2)); //cust username
				custList.add(res.getInt(4));	//username
				custList.add(res.getString(5));	//loan type
				custList.add(res.getString(6));	//loan status
				custList.add(res.getString(7)); //security qn
				custList.add(res.getString(8));	//security ans
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return custList;
	}
	
	public List<String> getPendingList() {
		List<String> pendList = new ArrayList<String>();
		try {
			String s = "select * from bank_customer where loan_status=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, "false");
			res = pstmt.executeQuery();
			
			while(res.next()) {
				pendList.add(res.getString(1)); //cust name
				pendList.add(res.getString(2)); //cust username
				pendList.add(res.getString(5)); //loan type
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return pendList;
	}
	
	public int updateStatus() {
		try {
			String s = "update bank_customer set loan_status=? where username=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, status);
			pstmt.setString(2, cusername);
			
			int row = pstmt.executeUpdate();
		    return row;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
}

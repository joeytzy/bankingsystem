package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class CustomerRegister
 */
public class CustomerRegister extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String cusername = request.getParameter("cusername");
		int password = Integer.parseInt(request.getParameter("password"));
		int balance = Integer.parseInt(request.getParameter("balance"));
		String securityQn = request.getParameter("securityQn");
		String securityAns = request.getParameter("securityAns");
		
		CustomerModel cm = new CustomerModel();
		cm.setCustName(name);
		cm.setCusername(cusername);
		cm.setPassword(password);
		cm.setBalance(balance);
		cm.setSecurityQn(securityQn);
		cm.setSecurityAns(securityAns);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("name", name);
		
		int register = cm.customerRegister();
		if(register == 1) {
			response.sendRedirect("/BankingSystem/CustRegSuccess.jsp");
		}
		else{
			response.sendRedirect("/BankingSystem/custRegFailure.html"); 
		}
	}

}

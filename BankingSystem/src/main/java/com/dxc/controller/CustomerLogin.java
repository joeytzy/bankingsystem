package com.dxc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class CustomerLogin
 */
public class CustomerLogin extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cusername = request.getParameter("cusername");
		int password = Integer.parseInt(request.getParameter("password"));
		
		CustomerModel cm = new CustomerModel();
		cm.setCusername(cusername);
		cm.setPassword(password);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("cusername", cusername);
		
		int login = cm.customerLogin();
		PrintWriter pw=response.getWriter();
		
		if(login == 1) {
			response.sendRedirect("/BankingSystem/custLoginSuccess.jsp");
		}
		else{
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Invalid credentials. Please try again!');");
			pw.println("location='/BankingSystem/custLogin.html';"); 
			pw.println("</script>");
		}
	}

}

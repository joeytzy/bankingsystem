package com.dxc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.AdminModel;

/**
 * Servlet implementation class ViewPending
 */
public class ViewPending extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		
		AdminModel am = new AdminModel();
		List<String> pendList = am.getPendingList();
		
		HttpSession session = request.getSession(true);
		if(pendList.size()>1) {
			session.setAttribute("data", pendList);
			response.sendRedirect("/BankingSystem/ViewPendingList.jsp");
		}
		else {
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Failed to retrieve pending list');");
			pw.println("location='/BankingSystem/adminLoginSuccess.jsp';"); 
			pw.println("</script>");
		}
	}

}

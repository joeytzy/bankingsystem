package com.dxc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.AdminModel;

/**
 * Servlet implementation class UpdateStatus
 */
public class UpdateStatus extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		
		String username = request.getParameter("username");
		String status = request.getParameter("status");
		
		AdminModel am = new AdminModel();
		am.setCusername(username);
		am.setStatus(status);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("username", username);
		
		int row = am.updateStatus();
				
		if(row==1) {
			response.sendRedirect("/BankingSystem/updateStatusSuccess.jsp");
		}
		else {
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Failed to update status');");
			pw.println("location='/BankingSystem/adminLoginSuccess.jsp';"); 
			pw.println("</script>");
		}
	}
}

package com.dxc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class ResetPassword
 */
public class ResetPassword extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("cusername");
		int newPassword = Integer.parseInt(request.getParameter("newPassword"));
		int cfmPassword = Integer.parseInt(request.getParameter("cfmPassword"));
		
		PrintWriter pw=response.getWriter();
				
		if(newPassword == cfmPassword) {
			CustomerModel cm = new CustomerModel();
			cm.setPassword(newPassword);
			cm.setCusername(username);
			int row = cm.resetPassword();
			if(row == 1) {
				pw.println("<script type=\"text/javascript\">");
				pw.println("alert('Password has been reset.');");
				pw.println("location='/BankingSystem/custLogin.html';"); 
				pw.println("</script>");
			}
			else {
				pw.println("<script type=\"text/javascript\">");
				pw.println("alert('Some problem occured. Try again!');");
				pw.println("location='/BankingSystem/resetPwd.html';"); 
				pw.println("</script>");
			}
		}
		else {
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Mismatch password, please give the correct password.');");
			pw.println("location='/BankingSystem/resetPwd.html';"); 
			pw.println("</script>");
		}
	}

}

package com.dxc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.AdminModel;

/**
 * Servlet implementation class AdminLogin
 */
public class AdminLogin extends HttpServlet {
	@Override
		protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		AdminModel am = new AdminModel();
		am.setUsername(username);
		am.setPassword(password);
		int login = am.adminLogin();
		
		HttpSession session = request.getSession(true);
		session.setAttribute("username", username); 
		
		if(login == 1) {
			response.sendRedirect("/BankingSystem/adminLoginSuccess.jsp"); 
		}
		else{
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Invalid credentials.\\nPlease try again');");
			pw.println("location='/BankingSystem/adminLogin.html';"); 
			pw.println("</script>");
		}

	}
}

package com.dxc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.TransactionModel;

/**
 * Servlet implementation class ViewTransaction
 */
public class ViewTransaction extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			PrintWriter pw=response.getWriter();
			
			HttpSession session = request.getSession(true);
	
			String username = session.getAttribute("cusername").toString();
			Date dateFrom = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("fromDate"));
			Date dateTo = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("toDate"));
			System.out.println(dateFrom.getTime() + dateTo.getTime());
			
			TransactionModel tm = new TransactionModel();
			tm.setCusername(username);
			tm.setFromDate(dateFrom);
			tm.setToDate(dateTo);
			ArrayList<Object> transaction = tm.viewTransaction();
			
			if(transaction.size() > 0) {
				session.setAttribute("transactionList", transaction);
				response.sendRedirect("/BankingSystem/ViewTransactionSuccess.jsp");
			}
			else {
				pw.println("<script type=\"text/javascript\">");
				pw.println("alert('Unable to get your balance');");
				pw.println("location='/BankingSystem/custLoginSuccess.jsp';"); 
				pw.println("</script>");
				
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}

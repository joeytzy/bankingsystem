package com.dxc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.AdminModel;
import com.dxc.model.TransactionModel;

/**
 * Servlet implementation class ViewCustomer
 */
public class ViewCustomer extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		
		AdminModel am = new AdminModel();
		List<Object> custList = am.getCustList();
		HttpSession session = request.getSession(true);
		if(custList.size()>1) {
			session.setAttribute("data", custList);
			response.sendRedirect("/BankingSystem/ViewCustomerList.jsp");
		}
		else {
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Failed to retrieve customer list');");
			pw.println("location='/BankingSystem/adminLoginSuccess.jsp';"); 
			pw.println("</script>");
		}
	}
}

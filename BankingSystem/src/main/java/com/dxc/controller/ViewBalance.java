package com.dxc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class ViewBalance
 */
public class ViewBalance extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String username = session.getAttribute("cusername").toString();
		
		PrintWriter pw=response.getWriter();
		CustomerModel cm = new CustomerModel();
		cm.setCusername(username);
		
		ArrayList<Integer> custList = cm.viewBalance();
		
		if(custList.size() > 0) {
			session.setAttribute("custList", custList);
			response.sendRedirect("/BankingSystem/ViewBalance.jsp");
		}
		else {
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Unable to get your balance');");
			pw.println("location='/BankingSystem/custLogin.html';"); 
			pw.println("</script>");
			
		}
		
	}
}

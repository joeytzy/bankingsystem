package com.dxc.controller;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class LoanApplication
 */
public class LoanApplication extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		HttpSession session = request.getSession(true);

		String username = session.getAttribute("cusername").toString();
		String loanType = request.getParameter("loan");
		
		CustomerModel cm = new CustomerModel();
		cm.setCusername(username);
		cm.setLoanType(loanType);
		int row = cm.applyLoan();
		if(row==1) {
			pw.println("<script type=\"text/javascript\">");
			pw.println("confirm('Your loan application has been submitted.');");
			pw.println("location='/BankingSystem/custLoginSuccess.jsp';"); 
			pw.println("</script>");
		}
		else {
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Please try again!');");
			pw.println("location='/BankingSystem/custLoginSuccess.jsp';"); 
			pw.println("</script>");
		}
		
		
	}

}

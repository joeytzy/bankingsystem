package com.dxc.controller;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.CustomerModel;

/**
 * Servlet implementation class SendMoney
 */
public class SendMoney extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		
		HttpSession session = request.getSession(true);
		String username = session.getAttribute("cusername").toString();
		String receiver = request.getParameter("receiver");
		int amount = Integer.parseInt(request.getParameter("amount"));
		
		CustomerModel cm = new CustomerModel();
		cm.setCusername(username);
		cm.setAmount(amount);
		cm.setReceiver(receiver);
		int res = cm.sendMoney();
		
		if(res==0) {
			response.sendRedirect("/BankingSystem/SendMoneySuccess.jsp");
		}
		else {
			pw.println("<script type=\"text/javascript\">");
			pw.println("alert('Transaction failed!\\nYou do not have enough money to make the transaction.');");
			pw.println("location='/BankingSystem/custLoginSuccess.jsp';"); 
			pw.println("</script>");
		}
	}

}
